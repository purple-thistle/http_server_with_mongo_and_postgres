//postgres in docker container
//create container docker run --name postgres -e POSTGRES_PASSWORD=mysecretpassword -p 5432:5432 -d postgres
//connect to db docker exec -it some-postgres psql -U postgres
//create table  CREATE TABLE accounts (
//   id SERIAL PRIMARY KEY,
//   username TEXT UNIQUE NOT NULL,
//   email TEXT NOT NULL
// );
//get the table SELECT * FROM accounts;

package main

import (
	"database/sql"
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "1"
	dbname   = "postgres"
)

type Postgresql struct {
	ConnStr string
}

// Set the PostgreSQL connection string
var connStr = fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
var pg = Postgresql{
	ConnStr: connStr,
}

func (p *Postgresql) Connect() (*sql.DB, error) {
	var err error

	// Connect to PostgreSQL
	db, err := sql.Open("postgres", pg.ConnStr)
	if err != nil {
		return nil, err
	}

	// Check the connection by pinging the server
	err = db.Ping()
	if err != nil {
		return nil, err
	}

	fmt.Println("Connected to PostgreSQL!")

	return db, nil
}

func (p *Postgresql) Hello(c echo.Context) error {
	_, err := pg.Connect()
	if err != nil {
		fmt.Println("Failed to connect to PostgreSQL:", err)
		return err
	}
	return c.String(http.StatusOK, "Hello, World!")
}

func (p *Postgresql) SaveUser(c echo.Context) error {
	// Get name and email
	name := c.FormValue("name")
	email := c.FormValue("email")

	db, err := pg.Connect()
	if err != nil {
		fmt.Println("Failed to connect to PostgreSQL:", err)
		return err
	}
	_, err = db.Exec("INSERT INTO accounts (username, email) VALUES($1, $2)", name, email)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	return c.String(http.StatusOK, "User name:"+name+", email:"+email+" is created")
}

func (p *Postgresql) GetUser(c echo.Context) error {
	// User ID from path `users/:name`
	name := c.Param("name")

	db, err := pg.Connect()
	if err != nil {
		fmt.Println("Failed to connect to PostgreSQL:", err)
		return err
	}

	rows, err := db.Query("SELECT email FROM accounts WHERE username = $1", name)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	defer rows.Close()

	emails := make([]string, 0)
	for rows.Next() {
		var email string
		err = rows.Scan(&email)
		if err != nil {
			panic(err)
		}
		emails = append(emails, email)
	}
	err = rows.Err()
	if err != nil {
		panic(err)
	}

	return c.String(http.StatusOK, "User name:"+name+", email:"+emails[0])
}

func (p *Postgresql) EditUser(c echo.Context) error {
	name := c.FormValue("name")
	email := c.FormValue("email")

	db, err := pg.Connect()
	if err != nil {
		fmt.Println("Failed to connect to PostgreSQL:", err)
		return err
	}

	sqlUpdate := `
	UPDATE accounts
	SET email = $1
	WHERE username = $2;`
	_, err = db.Exec(sqlUpdate, email, name)
	if err != nil {
		panic(err)
	}
	return c.String(http.StatusOK, "User "+name+", email:"+email+" is updated")
}

func (p *Postgresql) DeleteUser(c echo.Context) error {
	name := c.FormValue("name")

	db, err := pg.Connect()
	if err != nil {
		fmt.Println("Failed to connect to PostgreSQL:", err)
		return err
	}

	sqlDelete := `
	DELETE FROM accounts WHERE username = $1;`
	_, err = db.Exec(sqlDelete, name)
	if err != nil {
		panic(err)
	}

	return c.String(http.StatusOK, "User "+name+" is deleted")
}
