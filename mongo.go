//mongo docker container docker run --name mongodb -p 27017:27017 -d mongo
//connect docker exec -it mongo bash
//mongosh
//db show current database
//db.accounts.find() show all docs in acccounts collection

package main

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoDB struct {
	URI string
}

// Set the MongoDB URI
var uri = "mongodb://localhost:27017"

var mdb = MongoDB{
	URI: uri,
}

func (p *MongoDB) Connect() (*mongo.Client, error) {
	// Connect to MongoDB
	client, err := mongo.NewClient(options.Client().ApplyURI(mdb.URI))
	if err != nil {
		return nil, err
	}

	// Create a context and connect to MongoDB
	ctx := context.TODO()
	err = client.Connect(ctx)
	if err != nil {
		return nil, err
	}

	// Ping the MongoDB server to ensure that the connection is working
	err = client.Ping(ctx, nil)
	if err != nil {
		return nil, err
	}

	fmt.Println("Connected to MongoDB!")

	return client, nil
}

func (p *MongoDB) Hello(c echo.Context) error {
	// Connect to MongoDB
	client, err := mdb.Connect()
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(context.Background())

	return c.String(http.StatusOK, "Hello, World!")
}

func (p *MongoDB) SaveUser(c echo.Context) error {
	// Get name and email
	name := c.FormValue("name")
	email := c.FormValue("email")

	// Connect to MongoDB
	client, err := mdb.Connect()
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(context.Background())

	// Use the "test" database
	db := client.Database("users")

	// Get the "example" collection
	collection := db.Collection("accounts")

	// Check if the name already exists in the database
	filter := bson.M{"username": name}
	count, err := collection.CountDocuments(context.Background(), filter)
	if err != nil {
		return err
	}
	if count > 0 {
		return c.String(http.StatusBadRequest, fmt.Sprintf("A user with name %s already exists", name))
	}

	// Insert a document
	_, err = collection.InsertOne(context.Background(), map[string]string{"username": name, "email": email})
	if err != nil {
		return err
	}

	// Send a response
	return c.String(http.StatusOK, fmt.Sprintf("Inserted user %v with email %v into MongoDB!", name, email))
}

func (p *MongoDB) GetUser(c echo.Context) error {
	// Get the username from the request parameters
	name := c.Param("name")

	// Connect to MongoDB
	client, err := mdb.Connect()
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(context.Background())

	// Use the "test" database
	db := client.Database("users")

	// Get the "accounts" collection
	collection := db.Collection("accounts")

	// Find the user document with the specified name
	filter := bson.M{"username": name}
	var result bson.M
	err = collection.FindOne(context.Background(), filter).Decode(&result)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return c.String(http.StatusNotFound, fmt.Sprintf("User with name %s not found", name))
		}
		return err
	}

	// Send the user document as a response
	return c.JSON(http.StatusOK, result)
}

func (p *MongoDB) EditUser(c echo.Context) error {
	// Get name and email
	name := c.FormValue("name")
	email := c.FormValue("email")

	// Connect to MongoDB
	client, err := mdb.Connect()
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(context.Background())

	// Use the "test" database
	db := client.Database("users")

	// Get the "example" collection
	collection := db.Collection("accounts")

	// Check if the user exists in the database
	filter := bson.M{"username": name}
	count, err := collection.CountDocuments(context.Background(), filter)
	if err != nil {
		return err
	}
	if count == 0 {
		return c.String(http.StatusNotFound, fmt.Sprintf("User %s not found", name))
	}

	// Update the email for the user
	update := bson.M{"$set": bson.M{"email": email}}
	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		return err
	}

	// Send a response
	return c.String(http.StatusOK, fmt.Sprintf("Updated email for user %s to %s", name, email))
}

func (p *MongoDB) DeleteUser(c echo.Context) error {
	// Get name
	name := c.FormValue("name")

	// Connect to MongoDB
	client, err := mdb.Connect()
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(context.Background())

	// Use the "test" database
	db := client.Database("users")

	// Get the "example" collection
	collection := db.Collection("accounts")

	// Check if the user exists in the database
	filter := bson.M{"username": name}
	count, err := collection.CountDocuments(context.Background(), filter)
	if err != nil {
		return err
	}
	if count == 0 {
		return c.String(http.StatusNotFound, fmt.Sprintf("User %s not found", name))
	}

	// Delete the user
	_, err = collection.DeleteOne(context.Background(), filter)
	if err != nil {
		return err
	}

	// Send a response
	return c.String(http.StatusOK, fmt.Sprintf("Deleted user %s", name))
}
