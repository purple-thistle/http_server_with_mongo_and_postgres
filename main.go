package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	// Create a new Echo server
	e := echo.New()
	e.Use(middleware.Recover())

	db := new(Postgresql)
	db.Connect()

	// Define a handler function for the root route
	e.GET("/hello", db.Hello)
	e.POST("/users", db.SaveUser)
	e.GET("/users/:name", db.GetUser)
	e.PUT("/users", db.EditUser)
	e.DELETE("/users", db.DeleteUser)

	// Start the server
	e.Logger.Fatal(e.Start(":8080"))
}

type Database interface {
	Connect() (interface{}, error)
	SaveUser(c echo.Context) error
	GetUser(c echo.Context) error
	EditUser(c echo.Context) error
	DeleteUser(c echo.Context) error
	Hello(c echo.Context) error
}
